# API Play Bucket

## Autenticação

A autenticação é feita com Bearer token. Você deve enviar o token provido no header `Authorization`, desta forma:

`Authorization: Bearer <token>`

## Rotas

### Pastas

Retorna as pastas disponíveis.

`GET` `https://drivewell.playtelematics.com/api/v1/folders`

```json
{
  "data": [
    {
      "name": "PERSON_NAME",
      "folder": "FOLDER_NAME",
      "fleet": "FLEET_NAME"
    },
    { ... }
  ],
  "errors": []
}
```

<br/><br/><br/>

### Arquivos da pasta

Retorna o conteúdo da pasta requisitada. Utilize o nome de uma das pastas da requisição anterior no parâmetro `FOLDER_NAME` para ver os arquivos disponíveis.

`GET` `https://drivewell.playtelematics.com/api/v1/folders/${FOLDER_NAME}`

```json
{
  "data": [
    "rota1.tar.gz",
    "rota2.tar.gz",
    "rota3.tar.gz",
    "rota4.tar.gz",
    "rota5.tar.gz"
  ],
  "errors": []
}
```

<br/> <br/> <br/>

### Conteúdo do arquivo

Retorna o conteúdo do arquivo requisitado. Utilize o nome de algum dos arquivos `.tar.gz` obtidos na rota anterior para obter o JSON do arquivo mencionado.

`GET` `https://drivewell.playtelematics.com/api/v1/folders/${FOLDER_NAME}/${FILE_NAME}`

```json
{
  "data": { ... },
  "errors": []
}
```
